APP=keeplog
CC=gcc
LLIB=listlib
APP_HELPER=keeplog_helper

all: $(APP)

$(APP): $(APP).o $(APP_HELPER).o $(LLIB).o
	$(CC) -o $(APP) $(APP).o $(APP_HELPER).o $(LLIB).o

$(APP).o: $(APP).c $(APP_HELPER).h
	$(CC) -c $(APP).c

$(APP_HELPER).o: $(APP_HELPER).c $(LLIB).h
	$(CC) -c $(APP_HELPER).c

$(LLIB).o: $(LLIB).c $(LLIB).h
	$(CC) -c $(LLIB).c

clean:
	@-rm *.o $(APP)
